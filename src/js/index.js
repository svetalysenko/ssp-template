import $ from 'jquery'
import '@fancyapps/fancybox'
import Inputmask from 'inputmask'
import noUiSlider from 'nouislider'
import 'parsleyjs'
import 'select2'
import Swiper from 'swiper/bundle'
import 'tooltipster'
import './_backend'
import slick from 'slick-carousel'

$(function () {
  selects()
  inputMask()
  checkInputFill()
  validation()
  burgerMenu()
  sliderInit()
  headerDrop()
  headerAnonceDrop()
  servicesTabs()
  practiceMobMenu() 
})

window.addEventListener('load', function () {
  loader()
}, false)

function loader() {
  const
    loader = $('.loader'),
    body = $('body');
  body.removeClass('loading')
  loader.addClass('hidden')
}

function selects() {
  const selects = $('.ui-select select')
  selects.each(function () {
    const
      curr = $(this),
      currWrap = curr.parent('.ui-select')
    curr.select2({
      minimumResultsForSearch: Infinity,
      width: 'auto',
      dropdownAutoWidth: true,
      dropdownParent: currWrap
    })
  })
}

function inputMask() {
  Inputmask({
    'mask': '+7 (999) 999-99-99',
    'showMaskOnHover': false
  }).mask('#phone')
}

function checkInputFill() {
  $('input').on('change', function () {
    if ($(this).val() !== '') {
      $(this).addClass('filled')
    } else {
      $(this).removeClass('filled')
    }
  })
}

function validation() {
  $('form').parsley()
}

function burgerMenu() {
  $('.header-burger-button').click (function () {
    $(this).parents('.header').toggleClass('active');
  })
}

function sliderInit() {
  let swiperSlider = new Swiper('.swiper-container', {
    speed: 400,
    spaceBetween: 100,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  })

  $('.top-banner-slider').slick({
    arrows: false,
    autoplay: false,
    fade: true,
    dots: true
  })

  $('.practice-slider').slick({
    arrows: true,
    appendArrows: $('.practice-slider .slick-controls-arrows'),
    variableWidth: true,
    infinite: true,
    draggable: true,
    swipe: true,
    touchMove: true,
    swipeToSlide: true
  })
  
  $('.top-ban-slider').slick({
    arrows: false,
    autoplay: false,
    fade: true,
    dots: false
  })

  // $('.header-anonce-slider').slick({
  //   arrows: false,
  //   autoplay: false,
  //   fade: true
  // })
}

function headerDrop() {
  $('.header-menu-info-practice').click (function() {
    $(this).parents('.header-menu-info').toggleClass('active');
    $('.header-logo').toggleClass('active');
    $('header').toggleClass('subactive');
  })
}

function headerAnonceDrop() {
  const
    curr = $('.header-anonce-slider, .header-anonce-slider-bot'),
    btn = $('.anonce-btn');
  $('.anonce-btn').on('click', function () {
    curr.toggleClass('active');
  })
}

function servicesTabs() {
  const
    servicesTabToggle = $('.services-tabs-block-item-toggle');
  servicesTabToggle.on('click', function () {
    let curr = $(this),
      currDrop = curr.siblings('.services-tabs-block-item-drop');
    curr.toggleClass('active');
    currDrop.slideToggle(300);
    let tabsSlider = $('.services-tabs');
    setTimeout(function() {
      tabsSlider.slick("setOption", '', '', true);
  }, 500);
  })
}

function practiceMobMenu() {
  const
    practiceMobToggle = $('.section_title');
  practiceMobToggle.on('click', function () {
    let curr = $(this),
      currDrop = curr.siblings('.all-practice-box');
    curr.toggleClass('active');
    currDrop.slideToggle(300);
    let tabsSlider = $('.all-practice');
    setTimeout(function() {
      tabsSlider.slick("setOption", '', '', true);
  }, 500);
  })
}